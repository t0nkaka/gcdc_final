package com.osahub.hcs.vaccinate.services.dataaccess;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.osahub.ecomm.dao.shop.ProductDao;
import com.osahub.hcs.vaccinate.dao.AnswersRepository;
import com.osahub.hcs.vaccinate.dao.KnowledgeRepository;
import com.osahub.hcs.vaccinate.dao.VaccineRepository;
import com.osahub.hcs.vaccinate.dao.Vocabulary;
import com.osahub.hcs.vaccinate.dao.locator.VaccinationCenter;


public class OfyService {
    static {
        factory().register(ProductDao.class);
        factory().register(AnswersRepository.class);
        factory().register(KnowledgeRepository.class);
        factory().register(VaccineRepository.class);
        factory().register(Vocabulary.class);
        factory().register(VaccinationCenter.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}