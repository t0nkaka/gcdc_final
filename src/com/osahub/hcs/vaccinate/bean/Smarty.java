package com.osahub.hcs.vaccinate.bean;

import static com.osahub.ecomm.services.dataccess.OfyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import com.osahub.ecomm.services.dataccess.OfyService;
import com.osahub.hcs.vaccinate.dao.KnowledgeRepository;
import com.osahub.hcs.vaccinate.dao.Vocabulary;
import com.osahub.hcs.vaccinate.util.VacciBotUtil;

public class Smarty {
	//declaring and initialize with blank string
	public static String[] vocab = {"","","","","","","","","","","","",""}; 
	public ArrayList<Double> priorProbability;
	public String fillers = "each every its this that , . / ; ' [ ] \\ < > ? : \" { } | ! @ # $ % ^ & * ( ) _ + = -  is am are he she it they my your you using use used on at to for will shall have go gone went be in of do does yourself the a an no another some any our their her his 1 2 3 4 5 6 7 8 9 0 etc. etc";

	

	


    //method to split the Input String and Implement Bag of Words
	public ArrayList<String> extractWords(String source)
	{
		ArrayList<String> chunks = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(source);
		System.out.println("tockenizing String : "+source);
		String currentWord = "";
		while (st.hasMoreElements()) {
			currentWord = st.nextElement().toString(); 
			if(!fillers.contains(currentWord.toLowerCase())){
				chunks.add(currentWord);
			System.out.println("filtered word = "+currentWord);}
		}
		
		return chunks;
	}
	
	public String findCategory(List<String> processedWords)
	{
		return  VacciBotUtil.GetCategoryStringFromInt(getLikelyhood(processedWords)) ;
		
	}

	public int getLikelyhood(List<String> words)
	{
		return 1;
	}

/*
	public int getLikelyhood(List<String> words)
	{
		//length is 13 representing each class and initialize each element to 1 (if smooting) else to 0
		double likelyhood[] ={0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0} ;      
		//double pWords[words.size()] ;
		double pWords[] ;
		//int occuranceWords[words.size()] ;
		int occuranceWords[] ;
		int classWithMaxLikelyhood = 0;
		double temp = 0.0;
		//will iterate 13 times for each probability
        	for (int i=1; i < 14; i++){
    			//occuranceWords = null; //or intialize here to zero
    			int totalOccurance=0;
                	System.out.println("\n for iteration/ class = "+i);
    			int j=0;
    			String tempString = "";
    			for (String it : words)
    			{
    				//adding 1 for smoothing
    				int count = countOccurance( vocab[i], it)+1;
    				occuranceWords[j] = count;
                	
                		System.out.println("\n\toccurance of each word : "+count);		
    				
    				size_t found = tempString.find(*it);
      				if(found == String::npos)
      				{
    					totalOccurance = totalOccurance + count;
    					tempString = tempString+*it;
      				}

    				j++;
    			}
    				System.out.println("\n\t total occurance="+totalOccurance);
    				System.out.println("\n\t probability of each word is as follows");
    			for(int k=0; k<words.size(); k++)
    			{
    				if (occuranceWords[k] == 0)
    					pWords[k] = 0.0;
    				else
    					pWords[k] = (double)occuranceWords[k]/totalOccurance;
    				System.out.println("\n\t\t"+pWords[k]);
    				
    				if(pWords[k] > 0.0)
    				{
    					likelyhood[i] = 1.0;
    					likelyhood[i] = likelyhood[i] * pWords[k];
    				}
    			}

    			likelyhood[i] = likelyhood[i] * priorProbability[i];
                
    				System.out.println("\n\tPosterior Probability : "+likelyhood[i]);

    			if(temp < likelyhood[i])
    			{
    				classWithMaxLikelyhood = i;
    				temp = likelyhood[i];
    			}
    		}
		return classWithMaxLikelyhood;
	}*/
	

	public List<String> removeFillers(List<String> words){
		//List<String> processedWords;
		//return processedWords;
		return words;
	}

	public void creatingRepository(int catg) {
		KnowledgeRepository knowledge = null;
			knowledge = OfyService.ofy().load().type(KnowledgeRepository.class).id(catg).get();

			if (knowledge == null)
				knowledge = new KnowledgeRepository(catg, VacciBotUtil.GetCategoryStringFromInt(catg));

			knowledge.docCount = knowledge.docCount + 1;// this means we need not to verify the vocab input its going into DB with verified='true'
			// Now save the object
			ofy().save().entities(knowledge);
			ofy().clear();
	}
	

	public void creatingVocabulary(int catg, String content) {
		Vocabulary vocab = null;
		vocab = new Vocabulary(catg, content, "admin@osahub.com", new Date(), "admin@osahub.com", new Date());
		ofy().save().entities(vocab);
		ofy().clear();
	}
	

	/*public int countOccurance(int knowledgeRepository, String word) {
		
		KnowledgeRepository knowledge1 = null;


				knowledge1 = OfyService.ofy().load().type(KnowledgeRepository.class).id(knowledgeRepository).get();

				if (knowledge1 == null) {
					knowledge1 = new KnowledgeRepository(i, VacciBotUtil.GetCategoryStringFromInt(i));
					System.out.println("created knowledge repo = "+ knowledge1.description);
				}

				if (knowledge1.docCount != 0) {
					System.out.println("For " + knowledge1.description+ "\n\told PP = " + knowledge1.priorProbability + "\n\t doc count = " + knowledge1.docCount);
					knowledge1.priorProbability = (double) knowledge1.docCount / totalDocCount;
					System.out.println("\t new PP = " + knowledge1.priorProbability);
				} else {
					System.out.println("For " + knowledge1.description + "\n\told PP = " + knowledge1.priorProbability + "\n\t doc count = " + knowledge1.docCount + "\n\t so, new PP not calculated");
				}

				// Now save the object
				ofy().save().entities(knowledge1);
				ofy().clear();
		
	}*/

}
