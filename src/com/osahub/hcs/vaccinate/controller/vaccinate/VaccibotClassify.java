package com.osahub.hcs.vaccinate.controller.vaccinate;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.xmpp.Message;
import com.google.appengine.api.xmpp.MessageBuilder;
import com.google.appengine.api.xmpp.MessageType;
import com.google.appengine.api.xmpp.XMPPService;
import com.google.appengine.api.xmpp.XMPPServiceFactory;
import com.osahub.hcs.vaccinate.bean.Smarty;

public class VaccibotClassify extends HttpServlet {
	private static final XMPPService xmppService = XMPPServiceFactory.getXMPPService();

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Message message = xmppService.parseMessage(request);
		
		Smarty smarty = new Smarty(); 
		ArrayList<String> words = smarty.extractWords(message.getBody());
		
			Message reply = new MessageBuilder().withRecipientJids(message.getFromJid()).withMessageType(MessageType.NORMAL).withBody(smarty.findCategory((smarty.removeFillers(words)))).build();
			xmppService.sendMessage(reply);
			}
}