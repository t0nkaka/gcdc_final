package com.osahub.hcs.vaccinate.controller.locator;
import static com.osahub.hcs.vaccinate.services.dataaccess.OfyService.ofy;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.osahub.hcs.vaccinate.dao.locator.VaccinationCenter;;

@SuppressWarnings("serial")
public class SaveVaccinationCenter extends HttpServlet {
   
       
       
       public void doPost(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException {

                                               response.setContentType("text/html");
                                               PrintWriter out = response.getWriter();
                                               
                                               
                                               String name=request.getParameter("name");
                                               String centerName=request.getParameter("centerName");
                                               double xCor=Double.parseDouble(request.getParameter("xCor"));
                                               double yCor=Double.parseDouble(request.getParameter("yCor"));
                                               
                                               int pincode=Integer.parseInt(request.getParameter("pincode"));
                                               
                                               VaccinationCenter pin1 = new VaccinationCenter(name , pincode , xCor , yCor , centerName );
                                               //Now save the object  
                                               
                                               
                                               ofy().save().entities(pin1);
                                               ofy().clear();
                                                       
                                                                       
                                               System.out.println("pin = "+ pin1);        

                                               
                                               ///response.sendRedirect("http://www.localhost:8888/");

       
       
       }
       
}