package com.osahub.hcs.vaccinate.controller.locator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import com.osahub.hcs.vaccinate.dao.locator.VaccinationCenter;
import com.osahub.hcs.vaccinate.services.dataaccess.OfyService;
import static com.osahub.hcs.vaccinate.services.dataaccess.OfyService.ofy;	

@SuppressWarnings("serial")
public class FetchVaccinationCenter extends HttpServlet {

	List<Integer> pinCodeList = new ArrayList<Integer>();
	String centerName;
	double xCor;
	double yCor;
	int pinCode;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		
		
		int pincode = Integer.parseInt(request.getParameter("fetchpin"));
		int finalPincode;

		
		List<VaccinationCenter> cars = (List<VaccinationCenter>) ofy().load().type(VaccinationCenter.class).list();
		
		Iterator<VaccinationCenter> i = cars.iterator();

		int min = 0;

		int count = 1;

		pinCodeList.clear();
		while (i.hasNext())

		{
			
			int pin_db = (Integer) i.next().pincode;
			System.out.println("------- " + pin_db);
			if (count == 1) {
				min = Math.abs(pin_db - pincode);
				pinCodeList.add(pin_db);
			}

			else {
				if (min > Math.abs(pin_db - pincode)) {
					min = Math.abs(pin_db - pincode);
					pinCodeList.clear();
					pinCodeList.add(pin_db);

				}

				else if (min == Math.abs(pin_db - pincode)) {
					min = Math.abs(pin_db - pincode);
					pinCodeList.add(pin_db);

				}

			}

			System.out.println("Difference " + min +"pincode ="+pin_db);

			count++;

		}

		System.out.println("pincode list =  " + pinCodeList);
		finalPincode = pinCodeList.get(0);
		Query<VaccinationCenter> q = ofy().load().type(VaccinationCenter.class).filter("pincode =", finalPincode);
		
		Iterator<VaccinationCenter> ii = q.iterator();
		while(ii.hasNext()){
			
			
			VaccinationCenter pd = ii.next();
			centerName =pd.centerName;
			pinCode = pd.pincode;
			xCor = pd.xCor;
			yCor = pd.yCor;
		}
		
		request.setAttribute("centerName", centerName);
		request.setAttribute("pinCode", pinCode);
		request.setAttribute("xCor", xCor);
		request.setAttribute("yCor", yCor);
		request.getRequestDispatcher("map.jsp").forward(request, response);


	}

}