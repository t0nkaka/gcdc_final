package com.osahub.hcs.vaccinate.controller.register;


import static com.osahub.ecomm.services.dataccess.OfyService.ofy;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.osahub.ecomm.services.dataccess.OfyService;
import com.osahub.hcs.bean.mail.Sandesh;
import com.osahub.hcs.vaccinate.dao.registration.Person;

public class LoginServlet extends HttpServlet {
	String loginSource = null;
	Person p;
	HttpSession session ;
	
	public void doPost(HttpServletRequest request, HttpServletResponse resp) throws IOException {
		session = request.getSession();
		session.setAttribute("userId", null);
		
		UserService userService;
		String loggedInUser = request.getUserPrincipal().getName().toUpperCase();
		try {
		  	userService = UserServiceFactory.getUserService();
			
	        if (request.getUserPrincipal() == null){
	    		session.setAttribute("userId", null);
	        	resp.sendRedirect(userService.createLoginURL("/login"));
	        }
	        else{
				if(!isUserRegistered(loggedInUser)){
					registeruser(loggedInUser);
					sendWelcomeMail(loggedInUser);
				}
				session.setAttribute("userId", loggedInUser);
				resp.sendRedirect("/postLogin");
			}
		}catch(Exception err){}
		
	}
	
	public boolean isUserRegistered(String email){
		Person parent = OfyService.ofy().load().type(Person.class).id(email).get();
		if(parent == null)
			return false;
		else{
			parent.lastLogin = "Your last login : "+new Date();
			ofy().save().entities(parent);
			ofy().clear();
			return true;
		}
	}
	
	public void registeruser(String email){
		Person parent = null;
		parent = new Person(email, new Date(), 1 , "online@tikakaran.com", 1);
		ofy().save().entities(parent);
		ofy().clear();
	}
	
	public void sendWelcomeMail(String newUser){
		try{
			String mailBody="Dear User,\n\nThanks for registering with us.\n\nRegards,\nTeam Vaccinate";
			new Sandesh(newUser,newUser,"anshul@osahub.com","Team Vaccinate", "Welcome to Vaccinate.", mailBody).send();
		}catch(Exception m){}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException { 
		doPost(req, resp);
	}
}