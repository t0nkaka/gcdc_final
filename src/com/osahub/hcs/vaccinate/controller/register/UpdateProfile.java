package com.osahub.hcs.vaccinate.controller.register;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.LocalDate;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.Key;
import com.osahub.ecomm.services.dataccess.OfyService;
import com.osahub.hcs.vaccinate.dao.registration.Child;
import com.osahub.hcs.vaccinate.dao.registration.Person;

public class UpdateProfile extends HttpServlet {
	//Person p1;
	String  password;
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException { 
	    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		HttpSession session = req.getSession();

		String userId = session.getAttribute("userId").toString();
		Person parent =  OfyService.ofy().load().type(Person.class).id(userId).get();
		
		String servletAction = req.getParameter("action").toString();
		
		if( servletAction.equalsIgnoreCase("updatePersonDetails")){
	        String name = req.getParameter("name").toString();
	        String email = req.getParameter("email").toString();
	        String mobile = req.getParameter("mobile").toString();
	        String addressLine1 = req.getParameter("addressLine1").toString();
	        String locality = req.getParameter("locality").toString();
	        String district = req.getParameter("district").toString();
	        String city = req.getParameter("city").toString();
	        String state = req.getParameter("state").toString();
	        int pin = Integer.parseInt(req.getParameter("pin").toString());
	        //int country = Integer.parseInt(req.getParameter("country").toString());
	        
			Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
	        BlobKey image1 = blobs.get("image1_small");
	        parent.image1 = image1;
	        parent.email = email;
	        parent.mobile = mobile;
	        parent.addressLine1 = addressLine1;
	        parent.locality = locality;
	        parent.district = district;
	        parent.city = city;
	        parent.state = state;
	        parent.pin = pin;
	        //parent.country = country;
	        OfyService.ofy().save().entities(parent);
	        OfyService.ofy().clear();
	        resp.sendRedirect("/postLogin");
		}
		else if(servletAction.equalsIgnoreCase("addChild")){
			if(parent.childCount < 5){
				try{
					parent.childCount = parent.childCount+1;
			        OfyService.ofy().save().entities(parent);
			        OfyService.ofy().clear();
			        
					String cName = req.getParameter("cName").trim().toUpperCase();
					int year = Integer.parseInt(req.getParameter("ydob"));
					int month = Integer.parseInt(req.getParameter("mdob"));
					int day = Integer.parseInt(req.getParameter("ddob"));
					
					String dob = new LocalDate(year,month,day).toString();
					int age = getAge(day, month, year);
					
					Key<Person> pKey = Key.create(parent);
					Child c = new Child(pKey, cName, userId, dob, age);
			        OfyService.ofy().save().entities(c);
			        OfyService.ofy().clear();
					
					resp.sendRedirect("/postLogin?status=101#main-content");
				}catch(Exception e){
					e.printStackTrace();
					resp.sendRedirect("/postLogin?status=102");
				}
			}else{ 
				resp.sendRedirect("/postLogin?status=103");
			}

		}
		
		/*
		if( req.getParameter("registrarEmail") != null){
			sourcePage = "/home.jsp?id="+osahub.util.Security.encodeNumber(req.getParameter("registrarNumber"))+"&&";
			registredBy =  req.getParameter("registrarEmail");
			registrationMode = Integer.parseInt(req.getParameter("registrationMode"));//chk if integer coming or string
			
		}
		else{
			sourcePage = "/index.html";
			registredBy = "ADMIN@TIKAKARAN.COM";
			registrationMode = 1;
		}
		String mobile = req.getParameter("mobile").trim();
		String name = req.getParameter("name").trim().toUpperCase();
		String email = "";
		if( req.getParameter("email") != null && req.getParameter("email").trim().length() > 0)
			email = req.getParameter("email").trim().toUpperCase();
		else
			email = mobile+"@tikakaran.com";
		
		int year = Integer.parseInt(req.getParameter("ydob"));
		int month = Integer.parseInt(req.getParameter("mdob"));
		int day = Integer.parseInt(req.getParameter("ddob"));
		
		LocalDate dob = new LocalDate(year+"-"+month+"-"+day);
		
		
		
		int age = getAge(day, month, year);
		
		//to check if same parent-child combination exists
		if(childExists(mobile, cName)){
			resp.sendRedirect(sourcePage+"?status=000");
		}
		else
		{	
			String hash = new String(Hasher.CreateHash(mobile+cName));
			//to check if same parent exists
			if(parentExists(mobile)){
				p1.childCount = p1.childCount + 1;
				//to check number of children for this existing parent
				if(p1.childCount == 5)
					resp.sendRedirect(sourcePage+"?status=014");//msg:You can not add more than four children.
			}
			else{
				//create a new Parent
				p1 = new Person(email, name, mobile);
				p1.hash = hash;
				p1.registrationDate = new Date();
				p1.registrationMode = registrationMode;
				p1.registredBy = registredBy;
				p1.userName = email;
				p1.password = hash.substring(0, 8);
				this.password = hash.substring(0, 8);
				p1.childCount=1;
				p1.role = 1;
				if(registrationMode != 1){
					if( req.getParameter("addressLine1") != null && req.getParameter("addressLine1").trim().length() > 0)
						p1.addressLine1 = req.getParameter("addressLine1").trim();
					if( req.getParameter("locality") != null && req.getParameter("locality").trim().length() > 0)
						p1.locality = req.getParameter("locality").trim();
					if( req.getParameter("district") != null && req.getParameter("district").trim().length() > 0)
						p1.district = req.getParameter("district").trim();
					if( req.getParameter("city") != null && req.getParameter("city").trim().length() > 0)
						p1.city = req.getParameter("city").trim();
					if( req.getParameter("addressLine1") != null && req.getParameter("addressLine1").trim().length() > 0)
						p1.state = req.getParameter("state").trim();
					if( req.getParameter("pin") != null && req.getParameter("pin").trim().length() > 0)
						p1.pin = Integer.parseInt(req.getParameter("pin").trim());
				}
			}
			if(p1.childCount < 5){
				String mailBody="";
				//create a new Child
				Key<Person> pKey = Key.create(p1);
				Child c = new Child(pKey, cName, dob, age, hash);
				c.registrationDate = new Date();
				
			    if(!p1.emailVerified)
			    	mailBody="Dear "+name+",\n\nThanks for registering with Tikakaran for you child "+cName+".\n\nFollow this link to complete the registration\nurl: http://www.tikakaran.com/verify?id="+p1.hash+"\n\nTo ask any vaccination related query, you can directly speak with our vaccination \n expert at +91-9555505352 (10am to 5pm) or drop a e-mail at support@tikakaran.com.\n\nRegards,\nTeam Tikakaran.";
			    	//mailBody="Dear "+name+",\n\nThanks for registering with Tikakaran for you child "+cName+".\n\nYour login details are as follows:\nUsername : "+email+"\nPassword : "+this.password+"\n\nFollow this link to complete the registration\nurl: http://www.tikakaran.com/verify?id="+p1.hash+"\n\nTo ask any vaccination related query, you can directly speak with our vaccination \n expert at +91-9555505352 (10am to 5pm) or drop a e-mail at support@tikakaran.com.\n\nRegards,\nTeam Tikakaran.";
			    else
			    	//if an volunteer/cust care/admin register his/her child then control comes here
				   	mailBody="Dear "+name+",\n\nCongrats! You are successfully registered with Tikakaran for you child "+cName+".\n\nTo ask any vaccination related query, you can directly speak with our vaccination \n expert at +91-9555505352 (10am to 5pm) or drop a e-mail at support@tikakaran.com.\n\nThanks and Regards,\nTeam Tikakaran.";

				
				if(!email.equalsIgnoreCase(mobile+"@tikakaran.com") && req.getParameter("sendMail")!=null){//to check if email id is entered at the time of registration
					//Now send the verification Mail
					Properties props = new Properties();
					Session session1 = Session.getDefaultInstance(props, null);
				    Message msg = new MimeMessage(session1);
				    try{
					    msg.setFrom(new InternetAddress("admin@tikakaran.com", "Team Tikakaran"));
					    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email, "Dear "+name+"."));
					    msg.setSubject("Tikakaran confirmation mail");
					    msg.setText(mailBody);
					    Transport.send(msg);
				    }catch(MessagingException m){
				    	System.out.println("Messaging Exception in Register Servlet");
				    	m.printStackTrace();		    	
				    }
				} else{
					p1.emailAlert = false;
					p1.newsletter = false; 
					
				}
				//Now save both of them  
				ofy().save().entities(p1,c);
				ofy().clear();
				resp.sendRedirect(sourcePage+"?status=001");
			}
		}*/
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException { 
		resp.sendRedirect("/index.html?status=020");
	}
	
/*	
	
	public boolean childExists(String mobile, String cName){	
		String hash = Hasher.CreateHash(mobile+cName);
		Child p = OfyService.ofy().load().type(Child.class).filter("hash", hash).first().get();
		if (null != p)
			return true;
		return false;
	}
	
	public boolean parentExists(String mobile){	
		p1 =null;
		p1 = ofy().load().type(Person.class).id(mobile).get();
		if (null != p1)
			return true;
		return false;
	}*/

	public int getAge(int day,int month, int year)
	{
		Calendar rightNow = Calendar.getInstance(); 
		int m1 = rightNow.get(Calendar.MONTH)+1;
		int d1 = rightNow.get(Calendar.DAY_OF_MONTH);
		int y1 = rightNow.get(Calendar.YEAR);
		
		int m2=month;
		int d2= day;
		int y2=year;
		int aayu=0;
		aayu=aayu+365*(y1-y2)+30*(m1-m2)+(d1-d2);
		
		return aayu;
	}
}