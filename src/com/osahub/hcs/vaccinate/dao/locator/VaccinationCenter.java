package com.osahub.hcs.vaccinate.dao.locator;

import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class VaccinationCenter {

	@Id
	String name ;
	@Index public int  pincode;
	//GeoPtProperty gpt
	
	 public double xCor;
	 public double yCor;
	 public String  centerName;
	//-id logic same as volunteer -name -add1 -add2 -location -state -country -pin -google maps link ji -contact1 -contact2 -fax -website -email -timing (for vaccination) -facilities - ispaid - isVerified -type -hospital -clinic
	
	 
    
    public VaccinationCenter()
    {
    	
    }
    
    public VaccinationCenter(String name , int pincode , double xCor ,double yCor , String centerName)
    {
    	 this.name = name;
    	this.pincode = pincode;
    	this.xCor = xCor;
    	this.yCor = yCor;
    	this.centerName = centerName;
    	
        
       
    }

	
	
	
}
