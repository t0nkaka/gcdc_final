package com.osahub.ecomm.bean.product;

import java.util.List;

import com.osahub.ecomm.dao.shop.ProductDao;
import com.osahub.ecomm.services.product.ProductService;

public class ProductBackingBean {
	ProductDao product;
	ProductService service;
	
	public List<ProductDao> SearchProductByCategory(String productCategory){
		return service.inquireProductByCategory(productCategory);
	}
	
	public String getCategoryName(int productCategory){
		String category = "";
		switch (productCategory) {
		case 1:
			category =  "Sarees";
			break;
			
		case 2:
			category =  "Lehenga";
			break;
			
		case 3:
			category =  "Suits";
			break;
			
		case 4:
			category =  "Anarkali";
			break;
			
		case 5:
			category =  "Kurtis";
			break;

		default:
			break;
		}
		return category ;
	}
	
	public ProductBackingBean(){
		this.service = new ProductService();
	}	
}