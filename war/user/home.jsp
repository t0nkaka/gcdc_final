<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.osahub.hcs.vaccinate.dao.registration.Person" %>
<%@ page import="com.osahub.hcs.vaccinate.dao.registration.Child" %>
<%@ page import="com.osahub.hcs.vaccinate.services.dataaccess.OfyService" %>
<%@ page import="com.osahub.hcs.vaccinate.util.VacciBotUtil" %>

<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@ page import="com.google.appengine.api.images.Image" %>
<%@ page import="com.google.appengine.api.images.ImagesService" %>
<%@ page import="com.google.appengine.api.images.ImagesServiceFactory" %>
<%@ page import="com.google.appengine.api.images.Transform" %>
<%@ page import="com.google.appengine.api.blobstore.BlobKey" %>

<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.jdo.Query" %>
<%@ page import="javax.servlet.http.HttpServlet" %>
<%@ page import="java.io.IOException" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<%@ page import="javax.servlet.http.HttpServletResponse" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>  

<%!
	String roleString, mobile, lastLogin, name, registrarEmail, userId;
	Person p1;
	Child c;
	int childCount, role;
	UserService userService ;
	HttpSession session;
	String aevai;
	boolean noProfilePicture;
	String image1_small;
%>
<%	
	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	BlobKey blobKey;
	session = request.getSession();
	if(session.getAttribute("userId")!=null)
		userId = session.getAttribute("userId").toString();
	else
		response.sendRedirect("/err.jsp?reason=invalid or bad link.\n\n use this <a href=\"registration\">link</a> to login");
	
	try{
	  	userService = UserServiceFactory.getUserService();
	  	
		
		p1 = OfyService.ofy().load().type(Person.class).id(userId).get();
		
		
					if(p1.image1 != null){
						noProfilePicture =false;
			        	ImagesService imagesService = ImagesServiceFactory.getImagesService();
			        	image1_small = imagesService.getServingUrl(p1.image1);
					}else{
			        	noProfilePicture =true;
					}
		
		
		lastLogin = p1.lastLogin;
		name = p1.name;
		childCount = p1.childCount;
		registrarEmail = p1.email;
		if(childCount > 0){
			//Key<Person> pKey = Key.create(p1);
			//fetch child list and display accordilgly in first tab
		}
		
		role = p1.role;
		roleString = VacciBotUtil.GetRoleStringFromInt(role);
	} catch(Exception e){
		response.sendRedirect("/index.html?status=021"); //An error occured! please login again	
	} 
%>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Vaccinate : <%=roleString%> Home </title>

	<!-- Included CSS Files -->
  <link rel="stylesheet" href="user/stylesheets/app.css">
  <link rel="stylesheet" href="user/stylesheets/offcanvas.css">
  <link rel="stylesheet" href="user/stylesheets/foundation.css">
  <link rel="stylesheet" href="user/stylesheets/vaccinate.css">
  <link rel="stylesheet" href="user/stylesheets/googleapisfont.css">

  

  <script src="/user/javascripts/foundation/modernizr.foundation.js"></script>
  <!-- for date picker-->
  
	<script src="/user/script/jquery-1.9.1.js"></script>
	<script src="/user/script/jquery-ui.js"></script>
	<script src="/user/script/style1.js"></script>
	<script  >
	$(function() {
	$( "#datepicker" ).datepicker();
	});
	
	
	</script>
	<!--<link rel="stylesheet" href="/css/style.css" />-->
</head>

  <body id="page" >
	<!-- Header and Nav -->
  
  <div class="row">
    <div class="two columns">
      <h1><img src="/user/images/logo.png" width="300" height="75"/></h1>
    </div>
    
    
    
    <div class="ten columns" id = "nav-tab">
      <dl class="tabs pill">
      <%
		switch(role){
						case 1:
							//customer care
							out.println("<dd class=\"active\"><a href=\"#start\">Profile</a></dd>");
							out.println("<dd ><a href=\"#main-content\">VacciMonitor</a></dd>");
							out.println("<dd ><a href=\"#parentZone\">Parent Zone</a></dd>");
							out.println("<dd><a href=\"#liveConsult\">Consultation</a></dd>");
							out.println("<dd><a href=\"" + userService.createLogoutURL("/index.html?status=022") +  "\">Logout</a></dd>");
							break;
						case 2:
							//volunteer
							out.println("<dd><a href=\"#main-content\">Vaccination Schedule</a></dd>");
							out.println("<dd class=\"active\"><a href=\"#start\">Add new User</a></dd>");
							out.println("<dd><a href=\"" + userService.createLogoutURL("/index.html?status=022") +  "\">Logout</a></dd>");
							break;
						case 3:
							//customer care
							out.println("<dd><a href=\"#main-content\">Vaccination Schedule</a></dd>");
							out.println("<dd><a href=\"#start\">Add new User</a></dd>");
							out.println("<dd class=\"active\"><a href=\"#features\">Search</a></dd>");
							out.println("<dd><a href=\"#faq\">Open Queries</a></dd>");
							out.println("<dd><a href=\"" + userService.createLogoutURL("/index.html?status=022") +  "\">Logout</a></dd>");
							break;
							
						case 4:
							//admin
							out.println("<dd><a href=\"#main-content\">Vaccination Schedule</a></dd>");
							out.println("<dd class=\"active\"><a href=\"#start\">Add a Person</a></dd>");
							out.println("<dd><a href=\"#features\">Search</a></dd>");
							out.println("<dd><a href=\"#faq\">Open Queries</a></dd>");
							out.println("<dd><a href=\"" + userService.createLogoutURL("/registration") +  "\">Logout</a></dd>");
							break;
								
						//default:
							//response.sendRedirect("/registration");
							//break;
					}
		%>
      </dl>
   </div>
   <!-- 
    <div class="two columns" id = "social-tab">
      <dl class="tabs pill">
        <a href="http://www.facebook.com/" target="_blank"><img title="Facebook" src="/user/images/facebook.jpg" width="30" height="30" onclick="http://www.facebook.com/"/></a>
        <a href="http://twitter.com/" target="_blank"><img title="Twitter" src="user/images/Twitter.png" width="30" height="30" onclick="http://twitter.com/"/></a>
        <a href="http://plus.google.com/" target="_blank"><img title="Google+" src="user/images/google.jpg" width="30" height="30" onclick="http://plus.google.com/"/></a>
      </dl>
    </div>
    -->
  </div>
  
  <!-- End Header and Nav -->
  

  <!-- First Band (Slider) -->
  <!-- The Orbit slider is initialized at the bottom of the page by calling .orbit() on #slider -->
  <div class="row">
    <div class="twelve columns">
      <hr/>
	  <h5>Welcome <%=name%>.</h3>
	  <hr/ >
		<ul class="tabs-content ">  
				<li class="active"  id = "startTab"> 
        		<h3>Profile Information</h3>	
					<!--onsubmit="return checkform(this);")>-->
					<form  class="nice custom"  action="<%if(true) out.println(blobstoreService.createUploadUrl("/updateProfile"));%>" enctype="multipart/form-data"  method="post" onsubmit="return checkform(this);")>
        			<fieldset>
        			<input type="hidden" id="action" name="action" value="updatePersonDetails" />
        			
    					<legend>Personal Detail</legend>
							<div class="row">
								<div class="six columns">
									<%
										if(noProfilePicture){
											out.println("<img alt=\"profile_picture\" src=\"/user/images/default.jpg\"/>");
										}else{
											out.println("<img alt=\"profile_picture\" src=\""+image1_small+"=s200\" />");
										}
									%>
        							<label for="myFile">Change your profile picture</label>
									<label for="image1_small" class="error" id="file1Error">Please choose an image for your profile</label>
            						<input type="file" class="medium blue radius button" id="image1_small" placeholder="Small Product Image" name="image1_small">
        						</div>
								<div class="six columns">
									<div class="row">
										<div class="ten columns">
        									<label>Name</label>
        									<input type="text"  id="name" value="<%=p1.name%>" placeholder="Full Name" name = "name"/>
											<label for="name" class="error" id="nError">Please enter name in proper format</label>
        								</div>
        							</div>
									<div class="row">
										<div class="ten columns">
        									<label>Email ID</label>
        									<input type="text"  id="email" readonly="readonly" value="<%=p1.email%>" placeholder="Email (optional)" name = "email"/>
											<label for="email" class="error" id = "eError">Please Enter Correct Email address</label>
        								</div>
        							</div>
        							<div class="row">
										<div class="ten columns">
        									<label>Contact Number</label>
        									<input type="text" id="mobile" value="<%=p1.mobile%>" placeholder="Mobile Number" name = "mobile" />
											<label for="mobile" class="error" id="mError">Please Enter mobile number of 10 digit</label>
        								</div>
        							</div>
        						</div>
        					</div>
        			</fieldset>
        			</br>
        			</br>
        			<fieldset>
    					<legend>Address Details</legend>
							<div class="row">
								<div class="four columns">
        							<label>Address Line 1</label>
        							<input type="text" value="<%=p1.addressLine1%>" id="addressLine1" placeholder="Address Line1 (optional)" name = "addressLine1"/>
									<label for="addressLine1" class="error" id="naddressLine1">Please enter addressLine1</label>
        						</div>
								<div class="four columns">
        							<label>Locality</label>
        							<input type="text" value="<%=p1.locality%>" id="locality" placeholder="Locality (optional)" name = "locality"/>
									<label for="locality" class="error" id="localityError">Please enter locality</label>
        						</div>
								<div class="four columns">
        							<label>District</label>
        			<input type="text"  id="district" value="<%=p1.district%>" placeholder="District (optional)" name = "district"/>
        			<label for="district" class="error" id="districtError">Please enter district</label>
        						</div>
        						
        					</div>
							<div class="row">
								<div class="four columns">
        							<label>City</label>
        			<input type="text"  id="city" value="<%=p1.city%>" placeholder="City (optional)" name = "city"/>
        			<label for="city" class="error" id="cityError">Please enter district</label>
        						</div>
								<div class="four columns">
        							<label>State</label>
        			<input type="text"  id="state" value="<%=p1.state%>" placeholder="State (optional)" name = "state"/>
        			<label for="state" class="error" id="stateError">Please enter a valid state</label>
        						</div>
								<div class="four columns">
        							<label>PIN</label>
        			<input type="text"  id="pin" value="<%=String.format("%06d",p1.pin)%>" placeholder="PIN (optional)" name = "pin"/>
        			<label for="pin" class="error" id="pinError">Please enter a valid pin number of 6 characters</label>
        						</div>
        					</div>	
        			</fieldset>
        			
        			 
        	
        			<!--
        			<label for="tAndC2" class="error" id="tAndC2Error">Please rechecked the details once.</label>
        			<input type="checkbox" class="input-text four" id="tAndC2" name = "tAndC2" />
        			I have rechecked the details.
        			<br/>
        			-->
        			<br/>
					<input type="hidden" value="<%=registrarEmail%>" name="registrarEmail" />
					<input type="hidden" value="<%=mobile%>" name="registrarNumber" />
        			<input type="submit" class="medium blue radius button" value="Update Profile Information" />
        		</form>
					
					
        		
			</li>
		
			<li id = "main-contentTab">
		  		<div class="row">
		  		<%
		  			if(childCount == 0)
						out.println("<h4>You do not have any children registered with us.</h4>");
					else{
						
					out.println("<div class=\"six columns\">");
					out.println("<form class=\"nice custom\" action= \"\" method=\"post\" onsubmit =\"return checkBackForm(this);\">");
					out.println("<fieldset>");
        					out.println("<legend>Monitor a Child <//legend>");
								out.println("<div class=\"row\">");
									out.println("<div class=\"ten columns\">");
        								out.println("<label>Select child<//label>");	
										out.println("<select  id=\"childId\" name=\"childId\">");
											for(int i=0;i<childCount;i++){
												out.println("<option value=\"1\">child"+i+"<//option>");
											}
										out.println("</select>");
        							out.println("</div>");
        						out.println("</div>");
						
						out.println("<input type=\"submit\" class=\"medium blue radius button\" value=\"Start Monitoring\"/>");
						out.println("</fieldset>");
					out.println("</form>");
					out.println("</div>");
					}
		  		%>
					
					
					<!--onsubmit="return checkchildform(this);")> -->
						<form  class="nice custom" action="updateProfile" method="post" onsubmit="return checkchildform(this);")>
					<fieldset>
        					<legend>Add a Child </legend>
								<input type="hidden" id="action" name="action" value="addChild" />
								<div class="row">
									<div class="six columns">
        								<label>Child name</label>
        								<input type="text" id="cName" placeholder = "Child's name" name = "cName" />
        								<label for="cName" maxlength="50" class="error" id="cNError">Please enter  child's name</label>
        							</div>
        						</div>
        						<!--
								<div class="row">
									<div class="six columns">
        								<label>Child's Date Of Birth</label>
        								<label for="datepicker" class="error" id="cDError">Please enter  child's DOB</label>
        								<input type="text" id="datepicker" placeholder = "Child's DOB" name = "datepicker" />
        							</div>
        						</div>
        						-->
        						<div class="row" id ="children">
        								<label>Child's DOB in DD/MM/YYYY format</label>
        							<div class="four columns">
		        					<div class="four columns ">
		        						<label for ="ddob" class="error" id = "dbError">Please Enter correct Date</label>
		        						<input type="text" maxlength="2" id="ddob" placeholder="DD" name ="ddob"/>
		        					</div>
		        					<div class="four columns pull-two">
		        						<label for="mdob" class="error" id="mbError">Please Enter correct month</label>
		        						<input type="text" maxlength="2" id="mdob" placeholder="MM" name="mdob"/>
		        					</div>
		        					<div class="four columns pull-three">
		        						<label for="ydob" class="error" id="ybError">Please Enter correct year</label>
		        						<input type="text" maxlength="4" id="ydob" placeholder="YYYY" name="ydob"/>
		        					</div>
        							</div>
        				
        						<div class="four columns pull-four">
        							<label class="error" id ="dobError">Please enter correct date of birth</label>
        						</div>
	        	
        			</div>
        			<!--
        			<label for="tAndC" class="error" id="tAndCError">Please rechecked the details once.</label>
        			<input type="checkbox" class="input-text four" id="cName" name = "tAndC" />
        			I have rechecked the details.
        			<br/>
        			-->
        			<br/>
					<input type="hidden" value="<%=registrarEmail%>" name="registrarEmail" />
					<input type="hidden" value="<%=mobile%>" name="registrarNumber" />
        			<input type="submit" class="medium blue radius button" value="Add Child" />
        			
					</fieldset>
        		</form>	
					
				</div>
				<div class="row">	
				</div> 
	      	</li>
	      
	      	
	      	<li id ="parentZoneTab">
				
				<div class="row">
					<div class="twelve columns">
						<div class="panel callout">
							<h5> Chat with other online parents</h5>
							<p>ipsum.</p>
						</div>
					
					</div>
				</div>
				

			</li>
	      	
	      	<li id ="liveConsultTab">
				
				<div class="row">
					<div class="twelve columns">
						<div class="panel callout">
							<h5>Ask all your queries from our VacciExpert.</h5>
							<p>lorem.</p>
						</div>
					
					</div>
				</div>
				

			</li>
	      	<!--
	      	<li id ="featuresTab">parentZone
				
				<div class="row">
					<div class="twelve columns">
						<div class="panel callout">
							<h5> kutte</h5>
							<p>kamine.</p>
						</div>
					
					</div>
				</div>
				

			</li>
	      	
	      	<li id ="faqTab">
	      	
	      	<h3>Frequently Asked Questions about Vaccination</h3>
	      	<hr/ >
	      	<div class="twelve columns">
	      	</div>
	      	


			</li>
			 -->
	    
   
	  </ul>
	  
	  </div>
	  </div>
 
  
  <!-- Footer -->
  
  <footer class="row">
    <div class="twelve columns">
      <hr />
      <div class="row">
        <div class="six columns">
          <p>&copy; Vaccinate 2013</p>
        </div>
        <div class="six columns">
          <ul class="link-list right">
            <li><%=lastLogin%></li>
          </ul>
        </div>
      </div>
    </div> 
  </footer>
 

	<!-- Modal windows hidden -->
	<div id = "backJobModal" class="reveal-modal medium">
		<form class="nice custom" action= "backjob" method="post" onsubmit ="return checkBackForm(this);">
			<h3>What do you want to do</h3>
			<label for="jobId">I want to </label>
			<select id="jobId" name="jobId">
				<option value="1">Unsubscribe from Email alerts</option>
				<option value="2">Delete My data from the Website</option>
				<option value="3">Get the confirmation mail</option>
				
			</select>
			<label for="bEmail" class="error" id="bEmailError">Please enter correct Email id</label>
			<input type="text" class="four nice" placeholder="Your email id" name="email" id="bEmail"/>
			<input type="submit" class="round blue button small" value="Submit"/>
		</form>
	</div>
	
	
	<!-- modal window for success -->
	
	<div id ="successChildAddModal" class="reveal-modal medium">
		<h2>Congrats!</h2>
		<p>You have successfully registered a child .</p>
	</div>
	
	<div id ="errorChildAddModal1" class="reveal-modal medium">
		<h2>Error!</h2>
		<p>Something went wrongwhile adding a child . Please try again.</p>
	</div>
	
	<div id ="errorChildAddModal2" class="reveal-modal medium">
		<h2>Error!</h2>
		<p>You can not add more then 5 child.</p>
	</div>
	
	<div id ="successModal" class="reveal-modal medium">
		<h2>Good Work!</h2>
		<p>You just registered a person successfully.</p>
		<p>Now we are one step closer to our goal. Long way to go... :)</p>
	</div>
	
	<div id="privacyModal" class="reveal-modal small">
		<h3>Our Privacy Policy</h3>
		<hr/>
		<p>We do not and will not share your data with anyone. Period.</p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="creditModal" class="reveal-modal medium">
		<h3>People, who made it a reality!</h3>
			<hr/>
			<p><a href="http://facebook.com/anshulwall" target="_blank">Anshul Jain</a></p>
			<p><a href="http://facebook.com/arti.nayal" target="_blank">Arti Nayal</a></p>
			<p><a href="http://facebook.com/t0m.ashu" target="_blank">Ashutosh Singh</a></p>
			<p><a href="http://facebook.com/ashuwall" target="_blank">Ashutosh Verma</a></p>
			<p><a href="http://facebook.com/manish.bhatia.9803" target="_blank">Manish Bhatia</a></p>
			<p><a href="http://facebook.com/shivi.gupta.58" target="_blank">Shivam Gupta</a></p>
			<!--<p><a href="http://facebook.com/shivani.jain.503645" target="_blank">Shivani Jain</a></p>-->
		<div class="small blue button close-modal">Close</div>
	</div>
	
	
	
	<div id="UnsubscribeModal" class="reveal-modal small">
		<h5>You have been unubscribed now!. Thanks for believing in us!</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="invalidEmailModal" class="reveal-modal small">
		<h5 class="red">Invalid Email id. Your data doesn't exist with us.</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifySuccessModal" class="reveal-modal medium">
		<h5 class="red">Thanks! Your email id has been verified! You will start receiving mails now!</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyInvalidHashModal" class="reveal-modal medium">
		<h5 class="red">Invalid hash code. Please register again to get the verification mail.</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	
	
	<div id="verifyInvalidHash2Modal" class="reveal-modal medium">
		<h5 class="red">Invalid hash code. Please register again to get the verification mail.</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyChildLimitExceeded" class="reveal-modal medium">
		<h5 class="red">Error : You can not register more child.</h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyUnauthorizeAccess" class="reveal-modal medium">
		<h3>Login Error!</h3>
			<hr/>
			<p>
		<h5 class="red">You are not authorized to access this section.<a href="/logout"> Click here</a> to try again.</h5><br\></p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyUnexpectedError" class="reveal-modal medium">
		<h3>Error while validating!</h3>
			<hr/>
			<p>
		<h5 class="red">You are not authorized to access this section.<a href="/logout"> Click here</a> to try again.</h5><br\></p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyInvalidHash" class="reveal-modal medium">
		<h3>Error while validating!</h3>
			<hr/>
			<p>
		<h5 class="red">invalid or bad verification link. Please use the link provided in the verification mail.</h5><br\></p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyValidated" class="reveal-modal medium">
		<h3>Congrats!</h3>
			<hr/>
			<p>
		<h5 class="red">Your email id has been verified. You will start receiving reminders now.</h5><br\></p>
	</div>
	
	<div id="verifyLogin" class="reveal-modal medium">
		<h3>Error while Login!</h3>
			<hr/>
			<p>
		<h5 class="red">invalid or bad login link. Please try again.</h5><br\></p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="verifyRegister" class="reveal-modal medium">
		<h3>Error while Registering!</h3>
			<hr/>
			<p>
		<h5 class="red">invalid or bad registration link. Please try again.</h5><br\></p>
		<div class="small blue button close-modal">Close</div>
	</div>
	
	<div id="deleteUserModal" class="reveal-modal small">
		<h5>Your data has been deleted. Have a great day. </h5>
		<div class="small blue button close-modal">Close</div>
	</div>
	<div id="feedbackRecdModal" class="reveal-modal medium">
		<h5>Thanks for Contacting Us. We will get back to you as soon as possible.</h5>
		<div class="small blue button close-modal">Close</div>
	
	</div>
	
	<div id="errModal" class="reveal-modal medium">
		<h5>Sorry, But your are already registered.</h5>
		<div class="small blue button close-modal">Close</div>
	
	</div>

	<!-- Included JS Files -->
  <script src="user/javascripts/foundation/jquery.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.reveal.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.orbit.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.forms.js"></script>
  <script src="user/javascripts/foundation/jquery.placeholder.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.tooltips.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.alerts.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.buttons.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.accordion.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.navigation.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.mediaQueryToggle.js"></script>
  <script src="user/javascripts/foundation/jquery.foundation.tabs.js"></script>
  <script src="user/javascripts/foundation/jquery.offcanvas.js"></script>
  <script src="user/javascripts/foundation/app.js"></script>
  <!-- Put this above your </body> tag -->
<script type="text/javascript">
$(window).load(function() {
	$('.close-modal').click(function(){
		$('.close-modal').trigger('reveal:close');
	});
	
    $('#slider').orbit({
    	animationSpeed:1200,
    	advanceSpeed:5000
    	 	});
    
    
  });

function getParameterByName(name){
	  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	  var regexS = "[\\?&]" + name + "=([^&#]*)";
	  var regex = new RegExp(regexS);
	  var results = regex.exec(window.location.search);
	  if(results == null)
	    return "";
	  else
	    return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	

 
  
  $(document).ready(function(){
	 //$('.numbers') .forcenumeric();
		$('.error').hide();
		var status = getParameterByName('status')
		//child added succefully 
		if(status == '101'){
			$('#successChildAddModal').reveal();
		}
		
		//error while adding child  
		if(status == '102'){
			$('#errorChildAddModal1').reveal();
		}
		
		//error while adding child  
		if(status == '103'){
			$('#errorChildAddModal2').reveal();
		}
		
		if(status == '001'){
			$('#successModal').reveal();
		}
		if(status == '000'){
			$('#errModal').reveal();
		}
		if(status == '002'){
			$('#UnsubscribeModal').reveal();			
		}
		if(status == '003'){
			$('#deleteUserModal').reveal();
		}
		if(status == '005'){
			$('#feedbackRecdModal').reveal();
		}
		if(status == '009'){
			$('#invalidEmailModal').reveal();
		}
		if(status == '010'){
			$('#verifySuccessModal').reveal();
		}
		if(status == '011'){
			$('#verifyInvalidHashModal').reveal();
		}
		if(status == '012'){
			$('#verifyNotRegisteredModal').reveal();
		}
		if(status == '013'){
			$('#verifyInvalidHash2Modal').reveal();
		}
		if(status == '014'){
			$('#verifyChildLimitExceeded').reveal();
		}
		if(status == '015'){
			$('#verifyUnauthorizeAccess').reveal();
		}
		if(status == '016'){
			$('#verifyUnexpectedError').reveal();
		}
		if(status == '017'){
			$('#verifyInvalidHash').reveal();
		}
		if(status == '018'){
			$('#verifyValidated').reveal();
		}
		if(status == '019'){
			$('#verifyLogin').reveal();
		}
		if(status == '020'){
			$('#verifyRegister').reveal();
		}
		
		
		
		$('.backJob').click(function(){
			$('#backJobModal').reveal();
			
		});
		
		
		
  });
  
 function isEmail(email){
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
 }
 
 function isNumeric(num){
	 var regex = /^[0-9]+$/
	 return regex.test(num);
 }
  
 
 function checkFForm(){
	 $('.error').hide();

	//check for name
	 if($('#fName').val().length<3 || isNumeric($('#fName').val())){
			$('#fNameError').show();
			return false;
	}
		
	//check for email
	 if($('#fEmail').val().length<3 || !isEmail($('#fEmail').val())){
		$('#fEmailError').show();
		return false;
	}
		
	//check for number
	 if($('#fNumber').val().length<10 || $('#fNumber').val().length>12 || !isNumeric($('#fNumber').val())){
			$('#fNumberError').show();
			return false;
	}

	//check for message
	if($('#fQuery').val().length<3){
		 $('#fQueryError').show();
		 return false;
	 }
 }
 
 //back office form validation
 function checkBackForm(){
	 
	 $('.error').hide();
	 if($('#bEmail').val().length>3){
		 if(!isEmail($('#bEmail').val())){
			  $('#bEmailError').show();
			  return false;
		  }
	 }
	 else{
		 $('#bEmailError').show();
		 return false;
	 }
	 
	 
 } 
 
 //login form validation
 function checkLoginForm(){
 	  $('.error').hide();
 	  //check username
 	  if($('#username').val().length>2){
 			  
 		  if(!isEmail($('#username').val())){
 			  $('#eUsername').show();
 			  return false;
 		  }
 	  }else{
 			  $('#eUsername').show();
 			  return false;
 		  }		  
 	  
 	//check password
 	var mobile = $('#password').val();
 	
 	if(mobile.length < 8 ){
		$('#ePassword').show();
		return false;
 	}
 	  	
 	return true;
 }
 
  //registration forms validation
function checkform(){
	  $('.error').hide();
	  //check if name is valid and doesnot contain digits.
	  if($('#name').val().length<3 || isNumeric($('#name').val())){
		  $('#nError').show();
		  return false;
	  }
	  
	//check pin number
	var pin = $('#pin').val();
	
	if(pin.length == 6 || pin.length == 0){
	
		if(pin.length == 6 && !isNumeric(pin)){
			$('#pinError').show();
			return false;
		}	
			
	}
	else{
		$('#pinError').show();
		return false;
	}
	  
	  //check email
	  if($('#email').val().length>0 && !isEmail($('#email').val())){
		  $('#eError').show();
		  return false;
	  }	  
	  
	//check mobile number and its optional
	var mobile = $('#mobile').val();
	
	if(mobile.length == 0 || mobile.length == 10 ){
		if(mobile.length == 10 ){
			if(!isNumeric(mobile)){
				$('#mError').show();
				return false;
			}	
			return true;
		}
		return true;
	}
	else{
		$('#mError').show();
		return false;
	}
	return true;
}
  

 
  //child registration forms validation
function checkchildform(){
	  $('.error').hide();
  	//check Child's name
  	if($('#cName').val().length<3){
		  $('#cNError').show();
		  return false;
	  }
  	
  	//check day
  	dday = $('#ddob').val();
  	mday = $('#mdob').val();
  	yday = $('#ydob').val();
  	
  	
  	
  	if(!isNumeric(dday) || dday<1 || dday >31 || dday.length<1){
  		$('#dobError').show();
  		return false;
  	}
  	
  	
  	if(!isNumeric(mday) || mday<1 || mday >12 || mday.length <1){
  		$('#dobError').show();
  		return false;
  	}
  	
  	if(!isNumeric(yday) || yday<2005 || yday >2013 || yday.length<4){
  	    $('#dobError').show();
  		return false;
  	}
	return true;
}
	
	
</script>
  
  

</body>