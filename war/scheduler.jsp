<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>GCDC 2013 : Vaccinate|Scheduler</title>
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/camera.css">
		<link rel="stylesheet" href="css/form.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/forms.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/tms-0.4.1.js"></script>
		<script>
			$(document).ready(function(){
				$('.slider_wrapper')._TMS({
					show:0,
					pauseOnHover:false,
					prevBu:'.prev',
					nextBu:'.next',
					playBu:false,
					duration:800,
					preset:'fade',
					pagination:true,//'.pagination',true,'<ul></ul>'
					pagNums:false,
					slideshow:8000,
					numStatus:false,
					banners: 'fade',
					waitBannerAnimation:false,
					progressBar:false
				});
			});
			$(document).ready(function(){
				!function(){
			var map=[]
			 ,names=[]
			 ,win=$(window)
			 ,header=$('header')
			 ,currClass
			$('.content').each(function(n){
			 map[n]=this.offsetTop
			 names[n]=$(this).attr('id')
			})
			win
			 .on('scroll',function(){
				var i=0
				while(map[i++]<=win.scrollTop());
				if(currClass!==names[i-2])
				currClass=names[i-2]
				header.removeAttr("class").addClass(names[i-2])
			 })
			}(); });
					function goToByScroll(id){
				$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
				}
				$(document).ready(function(){
					$().UItoTop({ easingType: 'easeOutQuart' });
				});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
		
		<!-- for calender-->
		<script src="script/jquery-1.9.1.js"></script>
<script src="script/jquery-ui.js"></script>
<script src="script/style1.js"></script>
<script  >
 $(function() {
$( "#datepicker" ).datepicker({
changeMonth: true,
changeYear: true
});
});

</script>
<link rel="stylesheet" href="/css/calander-style.css" />
	</head>
	<body class="">
<!--==============================header=================================-->
		<header class="page1">
			<div class="container_12">
				<div class="grid_12">
					<h1><a href="#" onClick="goToByScroll('page1'); return false;"><img src="images/logo.png" alt="Gerald Harris attorney at law"></a></h1>
					<div class="menu_block">
						<nav class="">
							<ul class="sf-menu">
								<li class=""><a href="/index.html">Home </a> <strong class="hover"></strong></li>
								<li class="current men"><a onClick="goToByScroll('page5'); return false;" href="#">Scheduler</a> <strong class="hover"></strong></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</header>
<!--=======content================================-->
		<div id="page1" class="content">
			<div class="ic"></div>
			<div class="container_12"><div class="grid_12">
					<div class="slogan">
						<h3>VacciScheduler</h3>
						<div class="text1">
							Helps you generate and download customized vaccination schedule for your child
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="map">
					
					<div class="grid_4">
						<div class="text1">Get Vaccination Schedule</div>
					<form id="formd"  method="post" action="generateVaccSch" onsubmit="return jsCheck();">
						<label>Child's Name</label></br>
						<input type="text" name="childName" style="color: grey; background-color: lightblue" id="childName" >
						</br>
						<div class="clear"></div>
						<div class="clear"></div>
						<label>Child's Date Of Birth</label></br>
						<input type="text" style="color: grey; background-color: lightblue" id="datepicker" name="dateOfBirth" />
						<div class="btns">
							
							<a  class="link1" onclick="document.getElementById('formd').submit();">Generate Vaccination Schedule</a>
						</div>

					</form>
						
					</div>
					
					<div class="grid_5">
						<div class="text1">Features</div>
						- Generate and download customized vaccination schedule<div class="clear"></div>
						- Use of Google Calender API to add reminders to your Google Calender<div class="clear"></div>
						- No Registration/Login required<div class="clear"></div>
						- You can email the vaccination schedule to yourself<div class="clear"></div>
					</div>
					
					<div class="grid_3">
						<div class="text1">Steps</div>
						<div class="text2">
							- Enter Name of your child<div class="clear"></div>
							- Enter Date Of Birth of your child in MM/DD/YYYY format<div class="clear"></div>
							- Click on 'Generate Schedule'
						</div>
					</div>
				</div>
					</br>	</br>
				Note : As of now, we are using immunization schedule issued by Govt. of India and WHO for SEAR region.
			</div>
		</div>
		
		
<!--==============================footer=================================-->
		<footer>
			<div class="container_12">
				<div class="grid_12">
					<div class="copy">
						<a onClick="goToByScroll('page1'); return false;" href="#"><img src="images/footer_logo.png" alt=""></a>  &copy; 2013 | Privacy Policy <br> <br>.
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</footer>
	</body>
</html>
