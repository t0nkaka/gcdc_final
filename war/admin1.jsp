<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>GCDC 2013 : Vaccinate</title>
		<link rel="icon" href="images/favicon.ico">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/camera.css">
		<link rel="stylesheet" href="css/form.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.1.1.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/forms.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/jquery.equalheights.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.ui.totop.js"></script>
		<script src="js/tms-0.4.1.js"></script>
		<script>
			$(document).ready(function(){
				$('.slider_wrapper')._TMS({
					show:0,
					pauseOnHover:false,
					prevBu:'.prev',
					nextBu:'.next',
					playBu:false,
					duration:800,
					preset:'fade',
					pagination:true,//'.pagination',true,'<ul></ul>'
					pagNums:false,
					slideshow:8000,
					numStatus:false,
					banners: 'fade',
					waitBannerAnimation:false,
					progressBar:false
				});
			});
			
			$(document).ready(function()
			{
				!function()
				{
					var map=[] ,names=[] ,win=$(window) ,header=$('header') ,currClass
					$('.content').each(	function(n)
										{
			 								map[n]=this.offsetTop
			 								names[n]=$(this).attr('id')
										}
									  )
					win.on('scroll',function()
										{
											var i=0
											while(map[i++]<=win.scrollTop());
											if(currClass!==names[i-2])
												currClass=names[i-2]
											header.removeAttr("class").addClass(names[i-2])
			 							}
			 				)
				}();
			});
					function goToByScroll(id){
				$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
				}
				$(document).ready(function(){
					$().UItoTop({ easingType: 'easeOutQuart' });
				});
		</script>
		<!--[if lt IE 8]>
			<div style=' clear: both; text-align:center; position: relative;'>
				<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
					<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
				</a>
			</div>
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" media="screen" href="css/ie.css">
		<![endif]-->
	</head>
	<body class="">
<!--==============================header=================================-->
		<header class="page1">
			<div class="container_12">
				<div class="grid_12">
					<h1><a href="#" onClick="goToByScroll('page1'); return false;"><img src="images/logo.png" alt="Gerald Harris attorney at law"></a></h1>
					<div class="menu_block">
						<nav class="">
							<ul class="sf-menu">
								<li class="current men"><a onClick="goToByScroll('page1'); return false;" href="#">VacciBot </a> <strong class="hover"></strong></li>
								<li class="men1"><a onClick="goToByScroll('page2'); return false;" href="#">Lacator</a><strong class="hover"></strong></li>
								<!--
								<li class=" men2"><a onClick="goToByScroll('page3'); return false;" href="#">Services</a> <strong class="hover"></strong></li>
								<li class=" men3"><a onClick="goToByScroll('page4'); return false;" href="#">Clients</a> <strong class="hover"></strong></li>
								-->
								<li class=" men4"><a onClick="goToByScroll('page5'); return false;" href="#">Logout</a> <strong class="hover"></strong></li>
							</ul>
						</nav>
						<div class="clear"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</header>
<!--=======content================================-->
		<div id="page1" class="content">
			<div class="ic"></div>
			<div class="container_12">
				<div class="grid_12">
					<div class="slogan">
						<h3>VacciBot</h3>
						<div class="text1">
							Vaccibot is an INTELLIGENT Virtual Assistant whom you can ask all your vaccination related queries. 
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="map">
					<div class="grid_4">
						<div class="text1">Please help me learn!</div>
						
						<form name="vaccibotLearn" action="vaccibotLearn" method="post" onsubmit="return checkform(this);")>
							say something here<div class="clear"></div>
		        			<input type="text" id="content" name = "content"/><div class="clear"></div>
							choose a category<div class="clear"></div>
						 	<select  id="category" name="category">
  								<option value="1">Greetings</option>
  								<option value="2">General questions</option>
  								<option value="3">Abusive</option>
  								<option value="4">Suggestions</option>
  								<option value="5">Feedback</option>
  								<option value="6">Adieu</option>
  								<option value="7">Queries about vacc. schedule</option>
  								<option value="8">Queries : about next vaccine</option>
  								<option value="9">Queries how to do this and do that</option>
  								<option value="10">Queries how can i use schedular app</option>
  								<option value="11">Queries how can i use locator app</option>
  								<option value="12">Queries query about any particular vaccine</option>
  								<option value="13">Queries query about nearest vaccination center</option>
							</select><br></br>
							<input type="submit"  value="Teach VacciBot" />
		        		</form>
					</div>
					
					<div class="grid_4">
						
						<form name="loadVocabInMemory" action="loadVocabInMemory" method="post" onsubmit="return checkform(this);")>
							<input type="hidden" id="action" name="action" value="loadVocabInMemory" />
							<input type="submit"  value="Load Vocab In Memory" /></br>
		        		</form>
						
						<form name="loadVocabInMemory" action="loadVocabInMemory" method="post" onsubmit="return checkform(this);")>
							<input type="hidden" name="action" value="flushVocabInMemory" />
							<input type="submit"  value="Flush Vocab In Memory" /></br>
		        		</form>
						
						<form name="loadVocabInMemory" action="loadVocabInMemory" method="post" onsubmit="return checkform(this);")>
							<input type="hidden" name="action" value="checkForSync" />
							<input type="submit"  value="Check For Sync" /></br>
		        		</form>
		        		
						<div class="text1">Instructions...</div>
						- choose the category VERY carefully<div class="clear"></div>
						- please do not enter similar sentences<div class="clear"></div>
						- DO NOT use any special character (? ! .) at all <div class="clear"></div>
						- DO NOT give this link to anyone else<div class="clear"></div>
						- DO NOT use any short forms like TTYL LOL etc.<div class="clear"></div>
					
						<div class="text1">Examples...</div>
						- Greetings : Hi/Hello/good morning, how are you ?<div class="clear"></div>
						- General questions :  who are you? what u do?<div class="clear"></div>
						- Abusive : FCUK you AS*hOLE !<div class="clear"></div>
						- Suggestions : please add this feature as well<div class="clear"></div>
						- General questions :  who are you? what u do?<div class="clear"></div>
						- feedback : u guys are doing awesome job<div class="clear"></div>
						- Adieu : good bye or talk top you later<div class="clear"></div>
					</div>
					
					
					<div class="grid_4">
						<div class="text1">HIT Me!</div>
						<form name="vaccibotClassify" action="vaccibotClassify" method="post" onsubmit="return checkform(this);")>
							say something here<div class="clear"></div>
		        			<input type="text" id="input" name = "input"/><div class="clear"></div>
							</br>
							<input type="submit"  value="Test VacciBot" />
		        		</form>
					</div>

			</div>
		</div>
		<div id="page2" class="content">
			<div class="container_12">
				<div class="grid_12">
					<div class="slogan">
						<h3>Locator</h3>
						<div class="text1">
							Add a location Using this screen. 
						</div>
					</div>
				</div>
				
					<div class="grid_5">
					
					
					<form action="/savePincode" method="post">
  Pin name:<input type="text" name="name"/><br/><br/>
Pin Code:<input type="text" name="pincode"/><br/><br/>
x-coordinate:<input type="text" name="xCor"/><br/><br/>
y-coordinate:<input type="text" name="yCor"/><br/><br/>
veccination center name : <input type="text" name="centerName"/><br/><br/>

<input type="submit" value="login"/>
					
					
					
						
					</div>
			</div>
		</div>
		<!--
		<div id="page3" class="content">
			<div class="container_12">
				<div class="grid_12">
					<div class="slogan">
						<h3>Our Services</h3>
						<div class="text1">
							We provide following services, all of them are absolutely FREE!
						</div>
					</div>
				</div>
				<div class="grid_3">
					<div class="box maxheight1">
						<img src="images/box1_img1.png" alt="">
						<div class="text1"><a href="#">Register</a></div>You can register here for FREE to get timely vaccination reminders for your child via E-mail and SMS. You can also login to chat with other parents.
					</div>
				</div>
				<div class="grid_3">
					<div class="box maxheight1">
						<img src="images/box1_img2.png" alt="">
						<div class="text1"><a href="#">Locator</a></div>Locator helps you to search a nearest vaccination center in your vicinity. To search, enter you PIN Code and get relevant results.
					</div>
				</div>
				<div class="grid_3">
					<div class="box maxheight1">
						<img src="images/box1_img3.png" alt="">
						<div class="text1"><a href="#">Scheduler</a></div>Using Scheduler, you can generate and download customized vaccination schedule for your child, by entering DOB.
					</div>
				</div>
				<div class="grid_3">
					<div class="box maxheight1">
						<img src="images/box1_img4.png" alt="">
						<div class="text1"><a href="#">Consultation</a></div>Here you can ask all your vaccination related query from our virtual assistant, VACCIBOT. You can also chat with our vaccination expert.
					</div>
				</div>
			</div>
		</div>
		
		<div id="page4" class="content">
			<div class="container_12">
				<div class="grid_12">
					<h3>Our Clients</h3>
				</div>
				<div class="companies">
					<div class="grid_3">
						<img src="images/logo1.jpg" alt="">
						<div class="text1">Company Name</div>Aliquam nibh ante, egestas id dictum  wrecommodo luctus liberoaesent.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo2.jpg" alt="">
						<div class="text1">Company Name</div>Fusce adipiscing quam id risus gittis, non consequat lacus interdumoin.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo3.jpg" alt="">
						<div class="text1">Company Name</div>Quisque viverra nulla nunc, eu ultrices wertlibero ultricies egetasellus um.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo4.jpg" alt="">
						<div class="text1">Company Name</div>Courrewerwe abitur vel lorem sit amet ulla ullamcorper fermentumn vitae.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="clear"></div>
					<div class="grid_3">
						<img src="images/logo5.jpg" alt="">
						<div class="text1">Company Name</div>Oiliquam nibh ante, egestas id dictum  wrecommodo luctus liberoaesente.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo6.jpg" alt="">
						<div class="text1">Company Name</div>Gusce adipiscing quam id risus gittis, non consequat lacus interdumoiju.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo7.jpg" alt="">
						<div class="text1">Company Name</div>Niuisque viverra nulla nunc, eu ultrices wertlibero ultricies egetasellus ol.
						<br>
						<a href="#">Go to Site</a>
					</div>
					<div class="grid_3">
						<img src="images/logo8.jpg" alt="">
						<div class="text1">Company Name</div>Fouerourrewerwe abitur vel lorem sit amet ulla ullamcorper fermentum.
						<br>
						<a href="#">Go to Site</a>
					</div>
				</div>
			</div>
		</div>
		<div id="page5" class="content">
			<div class="container_12">
				<div class="grid_12">
					<div class="slogan">
						<h3>Get in Touch</h3>
						<div class="text1">
							Feel free to reach us, we will be happy to help.
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="map">
					<div class="grid_3">
						<div class="text1">Address</div>
						<address>
							<dl>
								<dt>Vaccinate<br>
								New Delhi,<br>
								India.
								</dt>
								<dd><span>Telephone:</span>- --- --- ----</dd>
								<dd><span>FAX:</span>- --- --- ----</dd></br>
								<dd>E-mail: <a href="anshul@osahub.com" class="link-1">anshul@osahub.com</a></dd>
								<dd>Website: <a href="http://osahub.com" target="_blank" class="link-1">www.osahub.com</a></dd>
							</dl>
						</address>
					</div>
					<div class="grid_3">
						<div class="text1">&nbsp;</div>
						<figure class="">
							<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=new+delhi&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=40.188298,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=New+Delhi,+West+Delhi,+Delhi,+India&amp;ll=28.635308,77.22496&amp;spn=0.349,0.676346&amp;t=m&amp;z=11&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=new+delhi&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=40.188298,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=New+Delhi,+West+Delhi,+Delhi,+India&amp;ll=28.635308,77.22496&amp;spn=0.349,0.676346&amp;t=m&amp;z=11" style="color:#0000FF;text-align:left">View Larger Map</a></small>
						</figure>
					</div>
					<div class="grid_5 prefix_1">
						<div class="text1">Ask a Question</div>
						<form id="form">
						<div class="success_wrapper">
						<div class="success">Contact form submitted!<br>
						<strong>We will be in touch soon.</strong> </div></div>
						<fieldset>
						<label class="name">
						<input type="text" value="Name:">
						<br class="clear">
						<span class="error error-empty">*This is not a valid name.</span><span class="empty error-empty">*This field is required.</span> </label>
						<label class="email">
						<input type="text" value="E-mail:">
						<br class="clear">
						<span class="error error-empty">*This is not a valid email address.</span><span class="empty error-empty">*This field is required.</span> </label>
						<label class="message">
						<textarea>Message:</textarea>
						<br class="clear">
						<span class="error">*The message is too short.</span> <span class="empty">*This field is required.</span> </label>
						<div class="clear"></div>
						<div class="btns"><a data-type="submit" class="link1">Send</a>
						<div class="clear"></div>
						</div></fieldset></form>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		
		-->
<!--==============================footer=================================-->
		<footer>
			<div class="container_12">
				<div class="grid_12">
					<div class="copy">
						<a onClick="goToByScroll('page1'); return false;" href="#"><img src="images/footer_logo.png" alt=""></a>  &copy; 2013 | Privacy Policy <br> <br>.
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</footer>
	</body>
</html>
